﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelProje.Models;

namespace HotelProje.Controllers
{
    public class MY_Log_InController : Controller
    {
        private My__ReserveEntities db = new My__ReserveEntities();

        // GET: MY_Log_In
        public ActionResult Index()
        {
            return View(db.Log_In.ToList());
        }

        // GET: MY_Log_In/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Log_In log_In = db.Log_In.Find(id);
            if (log_In == null)
            {
                return HttpNotFound();
            }
            return View(log_In);
        }

        // GET: MY_Log_In/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MY_Log_In/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Password,Gmail,ID")] Log_In log_In)
        {
            if (ModelState.IsValid)
            {
                db.Log_In.Add(log_In);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(log_In);
        }

        // GET: MY_Log_In/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Log_In log_In = db.Log_In.Find(id);
            if (log_In == null)
            {
                return HttpNotFound();
            }
            return View(log_In);
        }

        // POST: MY_Log_In/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Password,Gmail,ID")] Log_In log_In)
        {
            if (ModelState.IsValid)
            {
                db.Entry(log_In).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(log_In);
        }

        // GET: MY_Log_In/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Log_In log_In = db.Log_In.Find(id);
            if (log_In == null)
            {
                return HttpNotFound();
            }
            return View(log_In);
        }

        // POST: MY_Log_In/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Log_In log_In = db.Log_In.Find(id);
            db.Log_In.Remove(log_In);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
