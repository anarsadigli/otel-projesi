﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelProje.Models;

namespace HotelProje.Controllers
{
    public class MY_Reserve_TableController : Controller
    {
        private My__ReserveEntities db = new My__ReserveEntities();

        // GET: MY_Reserve_Table
        public ActionResult Index()
        {
            return View(db.Reserve_Table.ToList());
        }

        // GET: MY_Reserve_Table/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reserve_Table reserve_Table = db.Reserve_Table.Find(id);
            if (reserve_Table == null)
            {
                return HttpNotFound();
            }
            return View(reserve_Table);
        }

        // GET: MY_Reserve_Table/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MY_Reserve_Table/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Surname,Phone,Gmail,R_Date,Guest_number,State,Subject")] Reserve_Table reserve_Table)
        {
            if (ModelState.IsValid)
            {
                db.Reserve_Table.Add(reserve_Table);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reserve_Table);
        }

        // GET: MY_Reserve_Table/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reserve_Table reserve_Table = db.Reserve_Table.Find(id);
            if (reserve_Table == null)
            {
                return HttpNotFound();
            }
            return View(reserve_Table);
        }

        // POST: MY_Reserve_Table/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Surname,Phone,Gmail,R_Date,Guest_number,State,Subject")] Reserve_Table reserve_Table)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reserve_Table).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(reserve_Table);
        }

        // GET: MY_Reserve_Table/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reserve_Table reserve_Table = db.Reserve_Table.Find(id);
            if (reserve_Table == null)
            {
                return HttpNotFound();
            }
            return View(reserve_Table);
        }

        // POST: MY_Reserve_Table/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reserve_Table reserve_Table = db.Reserve_Table.Find(id);
            db.Reserve_Table.Remove(reserve_Table);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
