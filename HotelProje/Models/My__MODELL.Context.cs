﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HotelProje.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class My__ReserveEntities : DbContext
    {
        public My__ReserveEntities()
            : base("name=My__ReserveEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Log_In> Log_In { get; set; }
        public virtual DbSet<Reserve_Table> Reserve_Table { get; set; }
        public virtual DbSet<sign_up> sign_up { get; set; }
    }
}
