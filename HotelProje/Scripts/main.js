

	$(document).ready(function () {
		$(document).on("scroll", onScroll);
 
		$('a[href^="#"]').on('click', function (e) {
			e.preventDefault();
			$(document).off("scroll");
 
			$('a').each(function () {
				$(this).removeClass('navactive');
			})
			$(this).addClass('navactive');
 
			var target = this.hash;
			$target = $(target);
			$('html, body').stop().animate({
			    'scrollTop': $target.offset().top + 2
			}, 500, 'swing', function () {
			    window.location.hash = target;
			    $(document).on("scroll", onScroll);
			});
		});
	});

	$(function () {

	    $('#login-form-link').click(function (e) {
	        $("#login-form").delay(100).fadeIn(100);
	        $("#register-form").fadeOut(100);
	        $('#register-form-link').removeClass('active');
	        $(this).addClass('active');
	        e.preventDefault();
	    });
	    $('#register-form-link').click(function (e) {
	        $("#register-form").delay(100).fadeIn(100);
	        $("#login-form").fadeOut(100);
	        $('#login-form-link').removeClass('active');
	        $(this).addClass('active');
	        e.preventDefault();
	    });

	});

 
	function onScroll(event){
		var scrollPosition = $(document).scrollTop();
		$('.nav li a').each(function () {
			var currentLink = $(this);
			var refElement = $(currentLink.attr("href"));
			if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
				$('ul.nav li a').removeClass("navactive");
				currentLink.addClass("navactive");
			}
			else{
				currentLink.removeClass("navactive");
			}
		});
	
       
        $(function(){
            $('#portfolio').mixitup({
                targetSelector: '.item',
                transitionSpeed: 350
            });
        });

        //  //$(function() {
        //  //  $( "#datepicker" ).datepicker();
        //});
    
    };
