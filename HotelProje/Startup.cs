﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HotelProje.Startup))]
namespace HotelProje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
